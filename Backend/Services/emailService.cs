﻿using Backend.model;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;

namespace Backend.Services
{
    public class emailService : IEmailservice
    {
        private readonly IConfiguration _configuration;

        public emailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void sendEmail(emailDTO request)
        {
            var email = new MimeMessage();
            string bodyemail = "<center>" + request.Body + "</center>";

            email.From.Add(MailboxAddress.Parse("dummyemail3202@gmail.com"));
            email.To.Add(MailboxAddress.Parse(request.To));
            email.Subject = request.Subject;
            email.Body = new TextPart(TextFormat.Html) { Text = "<center>" + request.Body + "</center>" };

      
            using var smtp = new SmtpClient();
            smtp.Connect(_configuration.GetSection("EmailHost").Value, Convert.ToInt32(_configuration.GetSection("EmailPort").Value));
            smtp.Authenticate(_configuration.GetSection("EmailUserName").Value, _configuration.GetSection("EmailPassword").Value);
            smtp.Send(email);
            smtp.Disconnect(true);
        }
    }
}
