﻿using Backend.model;

namespace Backend.Services
{
    public interface IEmailservice
    {
        void sendEmail(emailDTO request);
    }
}
