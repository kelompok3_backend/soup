﻿namespace Backend.model
{
    public class invoiceModel
    {
        public string no_invoice { get; set; }
        public DateTime invoice_date { get; set; }
        public int tcourse { get; set; }
        public int tprice { get; set; }
    }
}