﻿namespace Backend.model
{
    public class categoryModel
    {
        public int id { get; set; }
        public string category_name { get; set; }

        public string picture { get; set; }

        public string description { get; set; }
    }
}
