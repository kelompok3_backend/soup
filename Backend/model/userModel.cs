﻿namespace Backend.model
{
    public class userModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string role { get; set; }
        public int isActive { get; set; }
    }
}
