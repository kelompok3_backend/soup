﻿namespace Backend.model
{
    public class emailDTO
    {
        public string? To { get; set; }
        public string? Subject { get; set; }
        public string? Body { get; set; }

    }
}
