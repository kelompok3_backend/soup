﻿namespace Backend.model
{
    public class productModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public decimal price { get; set; }
        public string picture { get; set; }
        public DateTime schedule { get; set; }
        public string description { get; set; }

        

    }
}
