﻿using Backend.model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class invoiceController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public invoiceController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        [Route("get_invoice")]
        public IActionResult GetInvoice(int user_id)
        {
            try {
                List<invoiceModel> invoices = new List<invoiceModel>();

                using(SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();

                    string query = "SELECT fk_user_id,invoice_id,invoice_date,b.tcourse,c.tprice FROM invoice a\r\n\t" +
                        "INNER JOIN (\r\n\t\t" +
                            "SELECT fk_invoice_id,COUNT(fk_invoice_id) AS tcourse FROM detail_invoice GROUP BY fk_invoice_id\r\n\t" +
                        ") b ON b.fk_invoice_id=a.invoice_id\r\n\t" +
                        "INNER JOIN (\r\n\t\t" +
                            "SELECT fk_invoice_id,SUM(b.price) AS tprice FROM detail_invoice a\r\n\t\t\t" +
                                "INNER JOIN course b ON b.id=a.fk_course_id\r\n\t\t\t" +
                                "GROUP BY fk_invoice_id\r\n\t" +
                        ") c ON c.fk_invoice_id=a.invoice_id";

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("user_id", user_id);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    sqlDataAdapter.Fill(dataTable);

                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dataTable.Rows[i]["fk_user_id"]) == user_id)
                        {
                            invoiceModel invModels = new invoiceModel();
                            invModels.no_invoice = dataTable.Rows[i]["invoice_id"].ToString() ?? string.Empty;
                            invModels.invoice_date = DateTime.Parse(dataTable.Rows[i]["invoice_date"].ToString() ?? string.Empty);
                            invModels.tcourse = Convert.ToInt32(dataTable.Rows[i]["tcourse"].ToString() ?? string.Empty);
                            invModels.tprice = Convert.ToInt32(dataTable.Rows[i]["tprice"].ToString() ?? string.Empty);
                            invoices.Add(invModels);
                        }
                    }
                }

                return Ok(invoices);
            }
            catch(Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { Message = "Soup Fatal Error."+ex });
            }
        }
    }
}
