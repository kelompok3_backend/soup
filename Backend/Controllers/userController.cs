﻿using Backend.model;
using Backend.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class userController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IEmailservice _emailservice;

        public userController(IConfiguration configuration, IEmailservice emailservice)
        {
            _configuration = configuration;
            _emailservice = emailservice;
        }

        #region Route
        [HttpPost]
        [Route("Login")]
        public IActionResult Login([FromBody] loginModel userLogin)
        {
            var user = ValidateUsers(userLogin);
            if(user != null)
            {
                return Ok("Login Success");
            }
            return NotFound("Login Failed");
        }

        [HttpPost]
        [Route("Register")]
        public IActionResult Register(userModel reg)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    
                    conn.Open();

                    string query1 = "SELECT * FROM [User]";
                    SqlCommand cmd1 = new SqlCommand(query1, conn);
                    cmd1.ExecuteNonQuery();
                    SqlDataAdapter dtadapter = new SqlDataAdapter(cmd1);
                    DataTable dt = new DataTable();
                    dtadapter.Fill(dt);

                    List<userModel> usr = new List<userModel>();

                    foreach (DataRow row in dt.Rows)
                    {
                        userModel u = new userModel();
                        u.id = Convert.ToInt32(row["id"].ToString());
                        u.name = row["name"].ToString() ?? string.Empty;
                        u.email = row["email"].ToString() ?? string.Empty;
                        u.password = row["password"].ToString() ?? string.Empty;
                        u.role = row["role"].ToString() ?? string.Empty;
                        u.isActive = Convert.ToInt32(row["isActive"].ToString());
                        usr.Add(u);
                    }

                    var currentUser = usr.FirstOrDefault(u => u.email == reg.email);

                    if(currentUser != null)
                    {
                        return Ok("Your Email Already Registered.");
                    }

                    string query = "INSERT INTO [User] VALUES('" + reg.name + "','" + reg.email + "','" + reg.password + "','" + reg.role + "',0)";
                    SqlCommand cmd2 = new SqlCommand(query, conn);
                    cmd2.ExecuteNonQuery();

                    conn.Close();
                }

                var token = GenerateToken(reg);

                emailDTO request = new emailDTO();
                request.To = reg.email.ToString();
                request.Subject = reg.name.ToString() + "'s Email verification";
                request.Body = "<a href=\"http://52.237.194.35:2028/api/user/verify?token=[LINK]\">Verify This Link</a>".Replace("[LINK]", token);
                _emailservice.sendEmail(request);

                return Ok("Register Successfully");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { Message = "Soup Fatal Error. "+ex });
            }
        }

        [HttpPost]
        [Route("ResetPass")]
        public IActionResult ResetPass(resetPassModel rpModel)
        {
            try
            {
                List<string> currEmail = new List<string>();

                using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();
                    string query = "SELECT email FROM [User]";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    SqlDataAdapter dtadapter = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    dtadapter.Fill(dt);

                    foreach (DataRow row in dt.Rows)
                    {
                        currEmail.Add(row["email"].ToString() ?? string.Empty);
                    }
                    conn.Close();
                }

                var currentUser = currEmail.FirstOrDefault(u => u == rpModel.email);

                if (currentUser == null)
                {
                    return Ok("Email not Registered");

                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                    {
                        conn.Open();
                        string query = "UPDATE [User] SET password='"+rpModel.newpass+"' WHERE email='"+rpModel.email+"'";
                        SqlCommand cmd = new SqlCommand(query, conn);
                        //cmd.Parameters.AddWithValue("email", rpModel.email);
                        //cmd.Parameters.AddWithValue("password", rpModel.newpass);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }

                return Ok("Password has Changed");
            }
            catch(Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { Message = "Soup Fatal Error. "+ex });
            }
            
        }

        [HttpGet]
        [Route("Verify")]
        public IActionResult Verify(string token)
        {
            bool validate = ValidateToken(token);
            if (validate)
            {
                var jwt = new JwtSecurityTokenHandler().ReadJwtToken(token);
                var user = jwt.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
                using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();
                    string query = "UPDATE [User] SET isActive=1 WHERE [name] = @name";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@name", user);
                    cmd.ExecuteNonQuery();
                    conn.Close();

                }
                return Ok("Your Account already active");
            }
            else
            {
                return Ok("You are not authorized");
            }
        }
        #endregion

        #region Private Function
        public userModel ValidateUsers(loginModel userLogin)
        {
            List<userModel> usr = new List<userModel>();

            using(SqlConnection conn=new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                string query = "SELECT * FROM [User]";
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataAdapter dtadapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                dtadapter.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {
                    userModel u = new userModel();
                    u.id = Convert.ToInt32(row["id"].ToString());
                    u.name = row["name"].ToString() ?? string.Empty;
                    u.email = row["email"].ToString() ?? string.Empty;
                    u.password= row["password"].ToString() ?? string.Empty;
                    u.role = row["role"].ToString() ?? string.Empty;
                    u.isActive = Convert.ToInt32(row["isActive"].ToString());

                    usr.Add(u);
                }
                conn.Close();
            }

            var currentUser = usr.FirstOrDefault(u => u.email == userLogin.Email && u.password == userLogin.Password);

            if (currentUser != null)
            {
                return currentUser;
            }

            return null;
        }

        private string GenerateToken(userModel user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.name),
                new Claim(ClaimTypes.Email, user.email),
                new Claim(ClaimTypes.Role, user.role)
            };

            var token = new JwtSecurityToken(
                _configuration["Jwt:Issuer"],
                _configuration["Jwt:Audience"],
                claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private bool ValidateToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var validationParameters = GetValidationParameters();

            SecurityToken validatedToken;
            try
            {
                IPrincipal principal = tokenHandler.ValidateToken(token, validationParameters, out validatedToken);
                if (principal.Identity.IsAuthenticated)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        private TokenValidationParameters GetValidationParameters()
        {
            return new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidIssuer = _configuration["Jwt:Issuer"],
                ValidAudience = _configuration["Jwt:Audience"],
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]))
            };
        }
        #endregion
    }
}
