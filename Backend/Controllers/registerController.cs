﻿using Backend.model;
using Backend.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class registerController : ControllerBase
    {
        #region Configuration
        private readonly IConfiguration _configuration;
        private readonly IEmailservice _emailservice;

        public registerController(IConfiguration config,IEmailservice emailservice)
        {
            _configuration = config;
            _emailservice = emailservice;
        }
        #endregion

        [HttpPost]
        [Route("Register")]
        public IActionResult Register(userModel reg)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    string query = "INSERT INTO [User] VALUES('" + reg.name + "','" + reg.email + "','" + reg.password + "','"+reg.role+"',0)";
                    conn.Open();

                    string query1 = "SELECT * FROM [User]";
                    SqlCommand cmd1 = new SqlCommand(query1, conn);
                    cmd1.ExecuteNonQuery();
                    SqlDataAdapter dtadapter = new SqlDataAdapter(cmd1);
                    DataTable dt = new DataTable();
                    dtadapter.Fill(dt);

                    SqlCommand cmd2 = new SqlCommand(query, conn);
                    cmd2.ExecuteNonQuery();
                    conn.Close();
                }

                var token = GenerateToken(reg);

                emailDTO request = new emailDTO();
                request.To = reg.email.ToString();
                request.Subject = reg.name.ToString() + "'s Email verification";
                request.Body = "<a href=\"http://52.237.194.35:2028/api/register/verify?token=[LINK]\">Verify This Link</a>".Replace("[LINK]", token);
                _emailservice.sendEmail(request);

                return Ok("Register Successfully");
            }
            catch(Exception ex)
            {
                return Ok(ex.ToString());
            }
        }

        [HttpGet]
        [Route("verify")]
        public IActionResult Verify(string token)
        {
            bool validate = ValidateToken(token);
            if (validate)
            {
                var jwt = new JwtSecurityTokenHandler().ReadJwtToken(token);
                var user = jwt.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
                using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();
                    string query = "UPDATE [User] SET isActive=1 WHERE [name] = @name";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@name", user);
                    cmd.ExecuteNonQuery();
                    conn.Close();

                }
                return Ok("Your Account already active");
            }
            else
            {
                return Ok("You are not authorized");
            }
        }

        #region Private Method
        private string GenerateToken(userModel user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.name),
                new Claim(ClaimTypes.Email, user.email),
                new Claim(ClaimTypes.Role, user.role)
            };

            var token = new JwtSecurityToken(
                _configuration["Jwt:Issuer"],
                _configuration["Jwt:Audience"],
                claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private bool ValidateToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var validationParameters = GetValidationParameters();

            SecurityToken validatedToken;
            try
            {
                IPrincipal principal = tokenHandler.ValidateToken(token, validationParameters, out validatedToken);
                if (principal.Identity.IsAuthenticated)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        private TokenValidationParameters GetValidationParameters()
        {
            return new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidIssuer = _configuration["Jwt:Issuer"],
                ValidAudience = _configuration["Jwt:Audience"],
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]))
            };
        }
        #endregion
    }
}
