﻿using Backend.model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        public IConfiguration _configuration;

        public ProductController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        #region GetAllProduct

        [HttpGet]
        [Route("get_products")]
        public IActionResult GetProducts(int limit, int course_id)
        {
            try
            {
                List<productModel> products = new List<productModel>();

                using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    conn.Open();
                    string query = "SELECT * FROM course INNER JOIN category ON course.fk_category_id=category.id";
                    SqlCommand cmd = new SqlCommand(query, conn);

                    if (course_id != 0)
                    {
                        query = "SELECT * FROM course INNER JOIN category ON course.fk_category_id=category.id WHERE id=@id";
                        cmd = new SqlCommand(query, conn);
                        cmd.Parameters.AddWithValue("id", course_id);
                    }

                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                    DataTable dataTable = new DataTable();
                    sqlDataAdapter.Fill(dataTable);

                    if (limit > 0)
                    {
                        for (int i = 0; i < limit && i < dataTable.Rows.Count; i++)
                        {
                            productModel pModels = new productModel();
                            pModels.id = Convert.ToInt32(dataTable.Rows[i]["id"].ToString());
                            pModels.name = dataTable.Rows[i]["name"].ToString() ?? string.Empty;
                            pModels.category = dataTable.Rows[i]["category_name"].ToString() ?? string.Empty;
                            pModels.picture = dataTable.Rows[i]["picture"].ToString() ?? string.Empty;
                            pModels.price = Convert.ToInt32(dataTable.Rows[i]["price"]);
                            pModels.description = dataTable.Rows[i]["description"].ToString() ?? string.Empty;
                            products.Add(pModels);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dataTable.Rows.Count; i++)
                        {
                            productModel pModels = new productModel();
                            pModels.id = Convert.ToInt32(dataTable.Rows[i]["id"].ToString());
                            pModels.name = dataTable.Rows[i]["name"].ToString() ?? string.Empty;
                            pModels.category = dataTable.Rows[i]["category_name"].ToString() ?? string.Empty;
                            pModels.picture = dataTable.Rows[i]["picture"].ToString() ?? string.Empty;
                            pModels.price = Convert.ToInt32(dataTable.Rows[i]["price"]);
                            pModels.description = dataTable.Rows[i]["description"].ToString() ?? string.Empty;
                            products.Add(pModels);
                        }
                    }
                }

                return Ok(products); // Jika berhasil, kirim respons OK dengan data produk.
            }
            catch (Exception ex)
            {
                // Tangani kesalahan dengan menambahkan pesan kesalahan ke log atau mengirim respon error ke klien.
                // Di sini, kita akan mengirim pesan kesalahan ke klien dalam bentuk JSON.
                return StatusCode(StatusCodes.Status500InternalServerError, new { Message = "Terjadi kesalahan saat mengambil data produk."+ex });
            }
        }
        #endregion

       
        #region get_detail_product
        [HttpGet]
        [Route("get_detail_product")]
        public productModel? GetDetailProduct(int id)
        {
            productModel prodItem = new productModel();

            string connectionString = _configuration.GetConnectionString("DefaultConnection");

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string query = "SELECT a.id,a.name,b.category_name,a.picture,a.price,a.description FROM course a INNER JOIN category b ON a.fk_category_id=b.id WHERE a.id=@id";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    prodItem = new productModel
                    {
                        id = Convert.ToInt32(reader["id"]),
                        name = reader["name"].ToString() ?? string.Empty,
                        category = reader["category_name"].ToString() ?? string.Empty,
                        picture = reader["picture"].ToString() ?? string.Empty,
                        price = Convert.ToInt32(reader["price"]),
                        description = reader["description"].ToString() ?? string.Empty
                    };
                }

                reader.Close();
            }

            return prodItem;
        }

        #endregion


        #region get_category_name
        [HttpGet]
        [Route("get_category_name")]
        public List<categoryModel> GetCategory(string category_name = null)
        {
            List<categoryModel> prodItem = new List<categoryModel>();

            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                string query = "SELECT * FROM category";

                if (!string.IsNullOrEmpty(category_name))
                {
                    query += " WHERE category_name = @category_name";
                }

                SqlCommand cmd = new SqlCommand(query, conn);

                if (!string.IsNullOrEmpty(category_name))
                {
                    cmd.Parameters.AddWithValue("@category_name", category_name);
                }

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    categoryModel category = new categoryModel
                    {
                        id = Convert.ToInt32(reader["id"]),
                        category_name = reader["category_name"].ToString(),
                        picture = reader["picture"].ToString(),
                        description = reader["description"].ToString()
                    };
                    prodItem.Add(category);
                }

                reader.Close();
            }

            return prodItem;
        }

        #endregion

        #region
        [HttpGet]
        [Route("get_category")]
        public List<categoryModel> GetCategory(int? limit = null)
        {
            List<categoryModel> prodItem = new List<categoryModel>();

            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                string query = "SELECT ";

                if (limit != null)
                {
                    query += $"TOP {limit} ";
                }

                query += "* FROM category";

                SqlCommand cmd = new SqlCommand(query, conn);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    categoryModel category = new categoryModel
                    {
                        id = Convert.ToInt32(reader["id"]),
                        category_name = reader["category_name"].ToString(),
                        picture = reader["picture"].ToString(),
                        description = reader["description"].ToString()
                    };
                    prodItem.Add(category);
                }

                reader.Close();
            }

            return prodItem;
        }

#endregion
    }
}
